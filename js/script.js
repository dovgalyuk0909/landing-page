class Main {
  constructor() {
    this.$blockPlay = document.querySelector('.play-block');
    this.$butPlay = document.querySelector('.button-play');
    this.$video = document.querySelector('.video-content');

    this.$butPlay.addEventListener('click',this.showVideo.bind(this));
  }
  showVideo() {
    this.$blockPlay.classList.add('none');
    this.$video.classList.remove('none');
  }
}

document.addEventListener('DOMContentLoaded',()=> {
    this.main = new Main();
});      